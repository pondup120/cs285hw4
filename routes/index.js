var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});
router.post('/result', function(req, res, next) {
 
  var from = req.body.departing
  
  console.log(req.body.departing);
  var to = req.body.returning
  var search_result_flights = search(from, to )
    res.render('list',{
      search_result_flights :  search_result_flights
    });
  });

  router.post('/passenger', function(req, res, next) {
    var fight = req.body.fight
    var total =parseInt(fight) +1000
      res.render('from',{
        flight :  fight,
        tax :1000,
        servicesfee : 0,
        discount : 0,
        total : total
      });
      // res.json({})
    });

module.exports = router;

function search(from, to ) {
  let search_result_flights = [];
  var fromA = from.split("(");
  var toA = to.split("(");
  
  var fromAirport = fromA[1].slice(0, 3);
  var toAirport = toA[1].slice(0, 3);
  for (let i = 0; i < allFlightDetails.length; i++) {
      if ((allFlightDetails[i].fromAirport === fromAirport) && (allFlightDetails[i].toAirport === toAirport)){
          search_result_flights.push(allFlightDetails[i]);
      }     
  }
  return search_result_flights;
}

var allFlightDetails = [
  {
      "price": 3519,
      "airline": "Singapore Air",
      "fromAirport": "BKK",
      "airportfrom" : "Suvarnabhumi",
      "toAirport": "CDG",
      "airportto" : "Paris - Charles-de-Gaulle",
      "takeoffTime": "19.05",
      "landingTime": "6.50",
      "travelHours": "17.45",
      "transitNumber": 2,
      "date": "Saturday, October 10 th, 2015", 
      "type" : "one-way",
      "transits": [
          {
              "transitAirport": "SIN",
              "transitCountry": "Singapore",
              "transitHours": 2.15
          },
          {
              "transitAirport": "MUC",
              "transitCountry": "Germany",
              "transitHours": 1.30
          }]
  },
  {
      "price": 2055,
      "airline": "Nok Air",
      "fromAirport": "CDG",
      "airportfrom" : "Paris - Charles-de-Gaulle",
      "toAirport": "DMK",
      "airportto" : "Donmuang",
      "takeoffTime": "20.05",
      "landingTime": "8.50",
      "travelHours": "13.45",
      "transitNumber": 1,
      "date": "Sunday, October 11 th, 2015", 
      "type" : "one-way",
      "transits": [
          {
              "transitAirport": "SIN",
              "transitCountry": "Singapore",
              "transitHours": 6.45
          }]
  },
  {
      "price": 2055,
      "airline": "Nok yuong",
      "fromAirport": "CDG",
      "airportfrom" : "Paris - Charles-de-Gaulle",

      "toAirport": "BKK",
      "airportto" : "Suvarnabhumi",
      "takeoffTime": "20.05",
      "landingTime": "8.50",
      "travelHours": "13.45",
      "transitNumber": 1,
      "date": "Monday, October 12 th, 2015", 
      "type" : "one-way",
      "transits": [
          {
              "transitAirport": "SIN",
              "transitCountry": "Singapore",
              "transitHours": 6.45
          }]
  },
  {
      "price": 99999,
      "airline": "Thai Air Asia",
      "fromAirport": "BKK",
      "airportfrom" : "Suvarnabhumi",
      "airportto" : "Paris - Charles-de-Gaulle",
      "toAirport": "CDG",
      "takeoffTime": "17.05",
      "landingTime": "6.50",
      "travelHours": "19.45",
      "transitNumber": 2,
      "date": "Saturday, December 13 th, 2015", 
      "type" : "one-way",
      "transits": [
          {
              "transitAirport": "DMK",
              "transitCountry": "Thailand",
              "transitHours": 4.15
          },
          {
              "transitAirport": "MUC",
              "transitCountry": "Germany",
              "transitHours": 1.30
          }]
  }
];
// {
//   "price": 99999,
//   "airline": "Thai Air Asia",
//   "fromAirport": "BKK",
//   "airportfrom" : "Suvarnabhumi",
//   "airportto" : "Paris - Charles-de-Gaulle",
//   "toAirport": "CDG",
//   "takeoffTime": "17.05",
//   "landingTime": "6.50",
//   "travelHours": "19.45",
//   "transitNumber": 2,
//   "date": "Saturday, December 13 th, 2015", 
//   "type" : "one-way",
//   "transits": [
//       {
//           "transitAirport": "DMK",
//           "transitCountry": "Thailand",
//           "transitHours": 4.15
//       },
//       {
//           "transitAirport": "MUC",
//           "transitCountry": "Germany",
//           "transitHours": 1.30
//       }]
// }