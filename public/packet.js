exports =  Package = function() {
    var services = [];
    this.addService = function(s) {
        services.push(s);
    };
    this.getTotalPrice = function(promo) {
        return promo.getTotalPrice(this);
    }

    this.getPrivilage = function(promo) {
        return promo.getPrivilage(this);
    }

    this.getAllService = function() {
        console.log("All Service: ");
        for (var i = 0; i < services.length; i++) {
            console.log(services[i]);
        }
    };

    this.containTrueMove = function() {
        for (var i = 0; i < services.length; i++) {
            if (services[i] instanceof TrueMove) {
                return true;
            }
        }
        return false;
    };

    this.containTrueVision = function() {
        for (var i = 0; i < services.length; i++) {
            if (services[i] instanceof TrueVision) {
                return true;
            }
            return false;
        }
    };

    this.containHighSpeed = function() {
        for (var i = 0; i < services.length; i++) {
            if (services[i] instanceof HighSpeed) {
                return true;
            }
            return false;
        }
    }
}

exports =  Baggage = function() {
    this.getPrice = function() {

        return 350;
    }
};

exports =  Insurance = function() {
    this.getPrice = function() {

        return 1300;
    }
};